const express = require('express');
const app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

var screenNumber = 0;
var clients = [];

app.get('/:id', function (req, res) {
    res.sendFile(__dirname + '/index.html');
    screenNumber = req.params.id
});

app.use(express.static(__dirname + '/'))

io.on('connection', function (socket) {
    console.log('a user connected',socket.id);
    clients.push({screen: screenNumber, id: socket.id});

    socket.on('cameraValues',msg =>{
        //console.log('controls',msg)
        io.emit('updatePosition',msg)
    })

    socket.emit('newScreen', {screen:screenNumber, qtdScreen: clients.length})

    if(clients.length > 1)
        io.emit('calcOffset', {qtdScreen: clients.length})
        
    socket.on('disconnect', () => {
        console.log("disconnected");
        clients.splice(clients.indexOf(socket.id), 1)
        //io.emit('newScreen', {screen:screenNumber, qtdScreen: clients.length})
        io.emit('calcOffset', {qtdScreen: clients.length})
      });
});

http.listen(3000, () => {
    console.log("listening to port: ", 3000);
})